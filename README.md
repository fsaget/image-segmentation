# BinIm : image segmentation

A Java implementation of a flow network based resolution of the image segmentation problem.

## Team members

- [Robin Pourtaud](https://gitlab.com/pourtaud) (M1 DS)
- [Félix Saget](https://gitlab.com/fsaget) (M1 VICO)

A full report on this project is available in the [rapport](rapport/rapport.pdf) directory (warning : in french).

## Executing & rebuilding

A JAR file is provided at the root of this repositery, containing the latest functional version of this code. Instructions below will help you execute this file, or rebuild it if you changed something in the code and want to try it out.

### Executing the provided JAR file

```bash
java -jar image-segmentation.jar
```

You will be prompted to either load the [default example file](examples/duck.txt), or load your own by providing its path.

### Creating a newer JAR executable

Follow the instructions in [this StackOverflow thread](https://stackoverflow.com/questions/4597866/java-creating-jar-file) to build your own from this project.