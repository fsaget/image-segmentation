---
documentclass: article
title: Binarisation d'image
subtitle: L'écriture numérique ouverte
author: Félix Saget et Robin Pourtaud
Affiliation: Université de Nantes   
date: 2021   
description: En traitement automatique d’images, il est essentiel de savoir identifier les diverses parties d’une image. A priori simple pour l’oeil humain, le problème d’identifier le premier plan d’une image, et son arrière-plan, n'est pas évident pour un ordinateur. Le but de ce projet est de définir une méthode pour séparer ces deux plans, basée sur la théorie des flots maximaux.
   
keywords: Théorie des Graphes
subject: écriture numérique ouverte
geometry:
- margin=2cm
- a4paper
fontsize: 11pt
toc: true
toc_depth:
---
# Réponses aux Questions
## Question 1 : 

Le problème de segmentation d'image consiste à mettre en évidence une **coupe** $(A,B)$ des pixels de l'image de manière à maximiser une fonction $q$ :

$$q(A,B) = \sum_{(i,j) \in A} a_{ij} + \sum_{(k,l)\in B }b_{kl} - \sum_{(i,j) \in A, (k,l)\in B \ \text{voisins}}p_{ijkl}$$

Où $a_{ij}$ et $b_{kl}$ correspondent aux **probabilités**, pour les pixels $[i,j]$ et $[k,l]$, d'être respectivement à l'arrière-plan ou au premier plan. $p_{ijkl}$ correspond à une **pénalité de séparation** entre pixels voisins, haute entre les pixels partageant le même plan, et basse aux délimitations entre les deux plans.

Ainsi, $q(A,B)$ est ici une fonction de **gain total**, qui récompense le placement correct des pixels :

- lorsqu'un pixel est placé en accord avec sa probabilité, il contribue à **maximiser** les deux premiers membres de $q(A,B)$ ;
- lorsque deux pixels voisins sensés appartenir au même plan sont séparés, leur haute valeur de pénalité contribue **négativement** à la minimisation du troisième membre de $q(A,B)$.

Ainsi, la maximisation de $q(A,B)$ permet d'évaluer la correction de la coupe $(A,B)$. Par commodité, essayons d'exposer une fonction $q'(A,B)$ permettant, par sa minimisation, d'assumer le même rôle que $q(A,B)$.

On pose $Q = \sum_{i=1}^n \sum_{j=1}^m (a_{ij} + b_{ij})$. Autrement dit, $Q$ est la **somme des probabilités** $a$ et $b$ de chaque pixel. Or, puisque $A \cap B = \emptyset$ :

$$Q = \sum_{(i,j) \in A}(a_{ij}+b_{ij}) + \sum_{(k,l) \in B}(a_{kl}+b_{kl})$$

Par ailleurs, on sait par l'énoncé que les relations suivantes s'appliquent pour tout pixel :

- $\forall (i,j)$, $a_{ij} = 1 \Leftrightarrow b_{ij} \geq 1$
- $\forall (i,j)$, $b_{ij} = 1 \Leftrightarrow a_{ij} \geq 1$

Alors, on peut réécrire $Q$ :

$$Q = \sum_{(i,j) \in A}(a_{ij}+1) + \sum_{(k,l) \in B}(1+b_{kl})$$

$$Q = (\sum_{(i,j) \in A}(a_{ij}) + \sum_{(k,l) \in B}(b_{kl})) + (\sum_{(i,j) \in A}(1) + \sum_{(k,l) \in B}(1))$$

$$Q = (\sum_{(i,j) \in A}(a_{ij}) + \sum_{(k,l) \in B}(b_{kl})) + (\sum_{(i,j) \in A}(b_{ij}) + \sum_{(k,l) \in B}(a_{kl}))$$


On peut séparer $Q$ en deux membres :

- le premier membre $Q_1$ effectue la **somme idéale** des probabilités, dans le cas où chaque pixel serait positionné dans le bon plan.
- inversement, le second membre $Q_2$ décompte donc le reste, le **pire cas**, la situation où chacun des pixels est mal placé.

De par les relations ci-dessus, on sait également que le membre gauche de $Q$ est en tout temps supérieur à son membre droit.

Notons que l'expression du membre gauche de $Q$ est retrouvée dans $q(A,B)$ : en effet, dans une optique de **maximisation**, on cherche à ce que le score de la coupe $(A,B)$ tende vers $Q_1$.

Ainsi, dans la fonction de minimisation $q'(A,B)$, on peut intégrer $Q_2$ en place de $Q_1$ dans $q(A,B)$. Alors :

$$q'(A,B) = Q_2 \pm \cdots $$

Pour ce qui est du décompte des **pénalités**, dans $q(A,B)$ on soustrayait au gain total les pénalités entre les pixels dans l'objectif qu'une pénalité entre deux pixels de même plan pénalise la maximisation de la fonction. Le principe est le même dans $q'(A,B)$ : on veut également que la minimisation soit affectée **négativement** par un mauvais choix de penalités. Alors, l'expression finale de $q'(A,B)$ est :

$$q'(A,B) = Q_2 + \sum_{(i,j) \in A, (k,l)\in B \ \text{voisins}}p_{ijkl}$$
$$q'(A,B) = \sum_{(i,j) \in A}(b_{ij}) + \sum_{(k,l) \in B}(a_{kl}) + \sum_{(i,j) \in A, (k,l)\in B \ \text{voisins}}p_{ijkl}$$

**Ainsi, la maximisation de $q(A,B)$ tout comme la minimisation de $q'(A,B)$ permettent d'évaluer si la coupe $(A,B)$ est idéale ou non.**

## Question 2 : 

À partir d'un jeu de données fourni, on obtient les propriétés suivantes :

- taille $n \times m$ de l'image
- pour chaque pixel $[i,j]$ de l'image :
	- la probabilité $a_{ij}$ pour le pixel d'être au premier plan
	- la probabilité $b_{ij}$ pour le pixel d'être à l'arrière-plan
- les pénalités $p_{ijkl}$ de séparation entre chaque paire de pixels voisins

Ces informations nous permettent de modéliser l'image par un réseau de transport de la forme suivante :

![Source : https://www.mdpi.com/2072-4292/13/17/3465/htm](/Users/robinpourtaud/Git/image-segmentation/rapport/schema.png)

_Commentaire : cette image provient d'un papier de recherche en Open Access et est libre de droits._

On ajoute deux sommets :

- le sommet $S$ correspond à la source. 
- le sommet $T$ correspond au puits. 

Les pixels sont reliés : 

- depuis $S$ par des arcs de capacité $a_{ij}$.
- à $T$ par des arcs de capacité $b_{ij}$. 
- à leurs pixels voisins par des arcs de capacité $p_{ijkl}$. 

$S$ et $T$ ne sont pas connectés entre eux. 

Lors de l'injection de flot dans le réseau de transport, la saturation des arcs reliant la source à chacun des pixels va générer un excédent proportionnel à la probabilité du pixel d'être à au premier plan, qui correspond à la capacité des arcs partant de $S$. Pour évacuer cet excédent, les pixels du premier plan, dont la probabilité d'être à l'arrière-plan (et donc la capacité des arcs vers le puits) est faible par construction, doivent transmettre le flot aux sommets de l'arrière-plan, qui ont une capacité plus importante à transmettre le flot au puits.

Au terme du calcul du flot maximal dans le réseau de transport, on pourra déterminer la solution au problème en exhibant la coupe (A,B) de la matrice de sommets, où :

- $A$ comprend l'ensemble des sommets accessibles directement depuis la source dans le réseau résiduel
- $B$ comprend le reste des sommets, ceux reliés directement au puits dans le réseau résiduel.

## Question 3 : 

Le théorème suivant établit un lien entre le calcul du flot maximal dans un réseau de transport et la coupe de capacité minimale de son graphe de base :

> **Théorème MaxFlow-MinCut** : pour tout digraphe $G$, tout couple $(s,t)$ de sommets, la valeur du flot maximal de $s$ à $t$ est égale à la capacité de la coupe minimale séparant $s$ de $t$. 

Ici, le graphe résiduel résultant du calcul de flot maximal constitue un digraphe : au terme du calcul, les sommets accessibles par la source ne peuvent pas accéder au puits. En effet, lorsque le flot est maximum, il n'existe plus de chemin de $S$ à $T$ dans le réseau résiduel (Graphes II & Réseaux, cours 1 "*Problème du flot maximum*", diapo 17).

Ainsi, si le flot calculé n'est pas minimal, il est possible qu'il reste un chemin de $S$ à $T$ dans le graphe résiduel : alors, il existe au moins un sommet accessible directement depuis la source également directement relié au puits dans le graphe résiduel. De par la définition de (A,B), le sommet en question devrait donc figurer à la fois dans $A$ et dans $B$. Or, si on a $A \cap B \neq \emptyset$, (A,B) n'est pas une coupe.

## Question 4 : 

La réponse à la question 4 est implémentée en Java 1.7 et se trouve dans l'archive. Nous avons décidé d'implémenter l'algorithme des préflots pour plusieurs raisons : 

- Ford Fulkerson peut ne pas converger.
- Edmonds-Karp converge toujours mais a une complexité temporelle en $\mathcal{O}(|S| \times |A|^2)$, alors que l'algorithme des préflots est en $\mathcal{O}(|S|^2\times |A|)$. Dans le cas de notre réseau, le nombre d'arcs est amplement supérieur au nombre de sommets : **algorithme des préflots préférable.** 

# Méthodes et algorithmes

## Algorithme principal

```pseudocode 
G <- ConstructionReseau()
CalculFlotMax(G)
Coupe <- CalculCoupeMin(G)
```
> **Description** : notre résolution du problème de segmentation d'image est divisée en trois étapes : l'initialisation du réseau de transport à partir du fichier donné, le calcul du flot maximal dans le réseau, et la mise en évidence de la coupe minimale.

## ConstructionReseau

- **Nom dans le code :** `FlowNetworkBin()` (constructeur de classe)
- **Entrées :**
	- *Entiers* n, m : dimensions de la matrice de pixels
	- *Tableaux 2D d'entiers* prob_a, prob_b : probabilités
	- *Tableaux 2D d'entiers* penalty_h, penalty_v : pénalités
- **Sortie :** un réseau de transport (source, tableau 2D de pixels, puits, arcs)
- **Complexité :** $O(n \times m)$ 

```
source, puits <- sommets vides
pixels <- Tableau2D(n,m)
pour i dans 1::n :
	pour j dans 1::m
		pixels[i][j] <- sommet
		créer un arc de la source au sommet, de capacité prob_a[]
		créer un arc du sommet au puits, de capacité prob_b[]
pour i dans 1::n :
	pour j dans 1::m :
		pour chaque voisin de pixels[i][j] :
			créer un arc du pixel à son voisin, de capacité 
			égale à la pénalité entre les deux sommets
```
> **Description :** initialise un réseau de transport à partir des informations données par un fichier.

## CalculFlotMax

Comme mentionné plus haut, nous avons décidé d'utiliser l'**algorithme des préflots** pour l'implémentaton de ce calcul. Les détails de l'implémentation sont dans la classe `PreflowPush`.

## CalculCoupeMin

- **Nom :** `displayResult`
- **Entrée :** un digraphe $G = (V,A)$
- **Sortie :** une coupe de $G$
- **Complexité :** $\mathcal{O}(V^2)$

```
A <- DFS(G)
B <- Ø
pour tout v de V :
	si !(v dans A) :
		mettre v dans B
retourner (A,B)
```

> **Description :** crée 2 ensembles disjoints $A$ et $B$ correspondant à la coupe de capacité minimale telle que $\text{source} \in A$ et $\text{puits} \in B$. Le contenu de $A$ est déterminé par un parcours des sommets accessibles directement depuis la source dans $G$ (`DFS`), de complexité $\mathcal{O}(V)$.

# Données de test et commentaires

Notre implémentation utilise l'algorithme des préflots : lors de nos essais, nous avons observé une source de lenteur pouvant être facilement évitée : 

- une fois que l'algorithme a envoyé le maximum au puit, il essaye de renvoyer les différents excédents des pixels à la source. Cependant la source étant de hauteur $|S|$ et les différents pixels à des hauteurs proches de 3 (dans nos différents essais), les pixels se partageaient les excédents entre eux tout en s'élevant de 1 pour chaque tour, ce qui rajoute de nombreuses opérations à l'algorithme.
- notre solution a été d'ajouter une variante de l'algorithme "Quick Mode" qui, au lieu d'initialiser la hauteur de la source au nombre de sommets, la fixe $\max \{m,n\}$, ce qui est un choix arbitraire. Nous avons essayé des valeurs plus petites, comme 4, qui pouvaient fonctionner pour certains fichiers, mais qui sur des exemples plus grands auraient pu altérer le comportement de l'algorithme. Notre choix permet une certaine adaptabilité à la taille des données d'entrée.

|Dataset|Quick mode|Normal mode|Flot maximal
|-|-|-|-|
4_demo| $<0.11s$ | $<0.1s$ | 93
4_example| $<0.1s$ | $<0.1s$ | 49
32_two-moons| $<0.5s$ | $<0.5s$ | 2'555
duck| $<0.1s$ | $10s$ | 7'004
Oublie| $<0.1s$ | $<0.1s$ | 2'809
tree| $4min$ | $\emptyset$ | 306'830

*Testé & mesuré sur un MacbookPro (M1, 8GB).*

![Résultat pour tree.txt](/Users/robinpourtaud/Git/image-segmentation/rapport/tree.png)

L'affichage peut varier en fonction de l'ordre des opérations *move* et *elevate*. Changer l'ordre a une influence sur le rendu ci-dessus, y-compris pour *duck.txt* : 



![Max Duck](/Users/robinpourtaud/Git/image-segmentation/rapport/finalDuck.png)



Saturer au maximum le graphe n'est pas forcément nécessaire pour obtenir un résultat "similaire". Par exemple, en modifiant quelques conditions et ayant un flux maximum moindre : 


![Different Duck](/Users/robinpourtaud/Git/image-segmentation/rapport/cuteDuck.png)



Il est également possible d'obtenir une approximation en modifiant certains points de l'algorithme : 

- envoi des excès de la source vers le puits dans le sens de stockages des arcs uniquement (pas de renvoi de l'excès des différents pixels)
- calcul approximatif de la coupe : on considère uniquement les arcs saturés des pixels vers le puits. Si saturé, alors le pixel appartient au premier plan, sinon au second. 

Ce qui nous donne : 


![Approx Duck](/Users/robinpourtaud/Git/image-segmentation/rapport/quickDuck.png)


> Commentaire : ceci est une capture d'écran ancienne, il faut inverser ici le premier plan et le second plan. 
