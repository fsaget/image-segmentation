package binim;
import java.util.Set;
import graph.EdgeFlow;
import graph.FlowNetworkBin;
import graph.VertexPreflow;
import graph.VertexPreflowPixel;
import graph.VertexPreflowSource;

public class PreflowPush {
    public FlowNetworkBin flow;
    
    public PreflowPush(FlowNetworkBin flow, boolean quickMode){
        assert flow != null;
        this.flow = flow;
        initPreflow(quickMode);
    }

    /**
     * Initialise the flow at the first step
     * @param flow the network
     */

    public void initPreflow(boolean quickMode){
        Integer nbVertice;
        if(quickMode){ 
            // Different for the initial algorithm
            // We have no proof that it will work all the time
            // However, it works for all of our dataset..
             nbVertice = Math.max(flow.getWidth(), flow.getHeight());
        }
        else{
            // Classic définition
             nbVertice = flow.getWidth()*flow.getHeight()+2;
        }
        flow.getSource().setHeight(nbVertice);
        flow.getSource().setExcess(0);

        flow.getSink().setHeight(0);
        flow.getSink().setExcess(0);


        for(int i=0; i<flow.getWidth(); i++){
            for(int j=0; j < flow.getHeight(); j++){
                flow.getPixel(i,j).setExcess(0);
                flow.getPixel(i,j).setHeight(0);
            }
        }

        Set<EdgeFlow> neighSource =  flow.getNeighborsEdges(flow.getSource());
        
        for(EdgeFlow e : neighSource){
            // Pixels initialisation
            Integer cap = e.getCurrentCapacity();
            
            e.setCurrentFlow(flow.getSource(),cap); // Inverse flow is not store but deduce
            flow.getSource().setExcess(-cap);
            e.getComplementValue(flow.getSource()).setExcess(cap);;
        }
    }
    /**
     * Move the excess from a Vertex (@param origin )
     * threw the edge (@param e)
     * @return boolean if we can move threw this edge
     */
    public boolean move(EdgeFlow e, VertexPreflow origin) {
        if(origin.getExcess() <= 0 || 
           origin.getHeight() != e.getComplementValue(origin).getHeight() + 1 ||
           e.getCapResiduel(origin) <= 0) 
        {
            return false;
        }
        Integer toSend = Math.min(origin.getExcess(), e.getCapResiduel(origin));
        e.addCurrentFlow(origin, toSend);
        origin.addExcess(-toSend);
        e.getComplementValue(origin).addExcess(toSend);

        return true;
    }
   

    /**
     * Increase the vertex (@param v ) height to the lowest possible value
     **/

    public boolean elevate(VertexPreflow v){
        if(v.getExcess() <= 0){
            return false;
        }
        Set<EdgeFlow> neighEdges= flow.getAccessibleNeighborsEdges(v);
        Integer min_height = Integer.MAX_VALUE;
        Integer current;
        for (EdgeFlow e : neighEdges){
            current = e.getComplementValue(v).getHeight();
            if ( current < min_height){
                min_height = current;
            }
        }
        if(min_height >= v.getHeight()){
            v.setHeight(1 + min_height);
            return true;
        }
        else{
            return false;
        }
        
    }

    /*
    * Main function of the preflow algorithm
    */
    public Integer computePreflow() {
        while (elevateIfPossible() || moveIfPossible()|| elevateSink() || moveBackToSource());
        flow.setMaximumFlowNumber(flow.getSink().getExcess());
        return flow.getSink().getExcess();
    }

    /*
    * Elevate one pixel if possible
    */
    public boolean elevateIfPossible(){
        boolean find = false;
        for(Integer i=0; i<flow.getWidth(); i++){
            for(Integer j=0; j<flow.getHeight(); j++){
                VertexPreflowPixel current = flow.getPixel(i, j);
                if (elevate(current)){
                    find = true;
                }
            }
        }
        return find;
    }
    /*
    * Move the excess of a pixel to another pixel or sink
    */
    public boolean moveIfPossible(){
        boolean find = false;
        for(Integer i=0; i<flow.getWidth(); i++){
            for(Integer j=0; j<flow.getHeight(); j++){
                VertexPreflowPixel current = flow.getPixel(i, j);
                
                for(EdgeFlow e : flow.getAccessibleNeighborsEdges(current)){
                    if(!(e.getComplementValue(current) instanceof VertexPreflowSource)){
                        if(move(e, current)){
                            find = true;
                        }
                    }
                }
                
            }
        }
        return find;
    }
    /*
    * Elevate the sink if necessary
    */
    public boolean elevateSink(){
        return elevate(flow.getSink());
    }
    /*
    * Move to the source only in last case
    */
    public boolean moveBackToSource(){
        boolean find = false;
        for(Integer i=0; i<flow.getWidth();i++){
            for(Integer j=0; j<flow.getHeight(); j++){
                if(move(flow.getSourceConnection(i, j), flow.getPixel(i,j))){
                    find = true;
                }
            }
        }
        return find;
    }
}