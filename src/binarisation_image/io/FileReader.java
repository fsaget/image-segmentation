package io;

import java.io.File;
import java.util.Scanner;
import graph.FlowNetworkBin;

/**
 *  Parses graph informations from a formatted text file.
 */
public class FileReader {
    public FlowNetworkBin parse(String filePath) throws RuntimeException {
        try {
            
            File f = new File(filePath);
            Scanner scn = new Scanner(f);
            
            // First line shall contain 2D dimensions n*m, formatted "n m"
            Integer size_n = 0;
            Integer size_m = 0;

            String tmp = scn.nextLine();
            String[] tmp_split = tmp.split(" ");  // returns ["n","m"]
            size_n = Integer.parseInt(tmp_split[0]);
            size_m = Integer.parseInt(tmp_split[1]);
            
            scn.nextLine();     // skipping an empty line

            // Parsing probabilities for each of the n*m vertices
            Integer[][] prob_a = new Integer[size_n][size_m];
            Integer[][] prob_b = new Integer[size_n][size_m];

            for(Integer i = 0; i < size_n; i++) {
                tmp = scn.nextLine();
                tmp_split = tmp.split(" ");

                for(Integer j = 0; j < size_m; j++) {
                    prob_a[i][j] = Integer.parseInt(tmp_split[j]);
                }
            }
            
            scn.nextLine();     // skipping an empty line

            for(Integer i = 0; i < size_n; i++) {
                tmp = scn.nextLine();
                tmp_split = tmp.split(" ");

                for(Integer j = 0; j < size_m; j++) {
                    prob_b[i][j] = Integer.parseInt(tmp_split[j]);
                }
            }

            scn.nextLine();     // skipping an empty line

            Integer[][] penalty_horizontal = new Integer[size_n][size_m-1];
            Integer[][] penalty_vertical = new Integer[size_n-1][size_m];

            for(Integer i = 0; i < size_n; i++) {
                tmp = scn.nextLine();
                tmp_split = tmp.split(" ");

                for(Integer j = 0; j < size_m-1; j++) {
                    penalty_horizontal[i][j] = Integer.parseInt(tmp_split[j]);
                }
            }
            
            scn.nextLine();     // skipping an empty line

            for(Integer i = 0; i < size_n - 1; i++) {
                tmp = scn.nextLine();
                tmp_split = tmp.split(" ");

                for(Integer j = 0; j < size_m; j++) {
                    penalty_vertical[i][j] = Integer.parseInt(tmp_split[j]);
                }
            }

            // EOF, parsing complete

            return new FlowNetworkBin(size_n, size_m, prob_a, prob_b, penalty_horizontal, penalty_vertical);
        } catch(Exception e) {
            /** The scanner will throw an exception if there are not enough lines
             * or if data is not formatted correctly. */
            throw new RuntimeException("File is not formatted correctly.");
        }
    }
}