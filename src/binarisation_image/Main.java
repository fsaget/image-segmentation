import java.util.Scanner;

import graph.*;
import io.FileReader;

public class Main {
    public static void main(String[] args) {
        /**
         * Options on launch :
         * - read file input
         * - prompt for input
         * 
         * Once input is read, graph is saved in memory :
         * - save graph as file (for prompted import)
         * - save network
         * - solve network for cut
         */

        /**
         * Direct usage :
         * java Main.java <path_to_graph_file>
         */
        // Mode Interactif
        System.out.println("Voulez-vous : ");
        System.out.println("1 - Charger l'image par défaut ? (Le canard)");
        System.out.println("2 - Charger une image dont vous spécifierez la source ?");
        Scanner sc = new Scanner(System.in);
        Integer input1 = Integer.parseInt(sc.nextLine());
        String path = "";
        FlowNetworkBin flowNetworkBin;
        if(input1 == 1){
            path = "examples/duck.txt"; 
            FileReader fileReader = new FileReader();
            flowNetworkBin = fileReader.parse(path);
            System.out.println("Pour essayer le mode rapide (détail dans le rapport) : 1, 2 sinon.");
            Integer input3 = Integer.parseInt(sc.nextLine());
            if (input3 == 1){
                flowNetworkBin.getMaximisedFlow(true).displayResult();
            }
            else{
                flowNetworkBin.getMaximisedFlow(false).displayResult();
            }
        }
        else{
            boolean fileLoaded = false;
            while(!fileLoaded){
                System.out.println("Localisation de votre fichier source : ");
                FileReader fileReader = new FileReader();
                String input2 = sc.nextLine();
                try {
                    flowNetworkBin = fileReader.parse(input2);
                    fileLoaded = true;
                    System.out.println("Pour essayer le mode rapide (risque faible d'échouer, détail dans le rapport) : 1, 2 sinon.");
                    Integer input3 = Integer.parseInt(sc.nextLine());
                    if (input3 == 1){
                        flowNetworkBin.getMaximisedFlow(true).displayResult();
                    }
                    else{
                        flowNetworkBin.getMaximisedFlow(false).displayResult();
                    }
                } catch(Exception e) {
                    System.out.println("Erreur lors de la lecture du fichier " + input2 + " : " + e.getMessage());
                }
            }         
        } 
        sc.close();
        
    }
}
