package graph;

public class EdgeFlow{
    // Un arc : Contient 2 sommets, possède une capacité et un arc
    private Integer currentFlow;
    private final Integer currentCapacity;
    private final VertexPreflow[] edge = new VertexPreflow[2];
    public EdgeFlow(VertexPreflow u,VertexPreflow v, Integer f, Integer c){
        this.edge[0] = u;
        this.edge[1] = v;
        this.currentFlow = f;
        this.currentCapacity = c;
    }
    public Integer getCurrentCapacity(){return this.currentCapacity;};

    public Integer getCurrentFlow(VertexPreflow val){
        if (edge[0] == val){
            return this.currentFlow;
        }
        else{
            // Si on est dans le mauvais sens, si val est la deuxième valeur de l'arc, alors on veut le flot negatif
            return -this.currentFlow; 
        }
    }
    
    public Integer getCapResiduel(VertexPreflow val){
        if (edge[0] == val){ // Si l'edge est dans le bon sens, c.a.d. val est bien l'origine de l'edge
            // Définition classique de la capcité résiduel
            return this.currentCapacity - this.currentFlow;     
        }
        else{
            // Sinon
            if(this.currentFlow > 0) { // Capacité résiduel sens inverse
                return  this.currentFlow;
            }
            else if(this.currentFlow < 0){ // Si le flot est négatif, alors on est dans le bon sens ! 
                return this.currentCapacity + this.currentFlow;
            }
            else{ // Aucun flot ne passe à travers, dans ce cas, il y a la capacité de disponible pour passer
                return this.currentCapacity;
            }
            
        }
    }
    
    public void setCurrentFlow(VertexPreflow val,Integer flow){
        // Le flot de (a,b) est stocké directement
        // Le flot de (b,a) est induit 
        if (edge[0] == val){ 
            this.currentFlow = flow;
        }
        else{
            this.currentFlow = - flow;
        }
    };

    public void addCurrentFlow(VertexPreflow val,Integer flow){
        if(val == this.edge[0]) {
            this.currentFlow += flow;
        } else {
            this.currentFlow -= flow;
        } 
    }

    
    public VertexPreflow getComplementValue(VertexPreflow val){
        // Pour (a,b), si val = a, alors donne le complémentaire : b
        // Sinon a
        if (edge[0] == val){
            return edge[1];
        }
        else{
            return edge[0];
        }
    }
}
