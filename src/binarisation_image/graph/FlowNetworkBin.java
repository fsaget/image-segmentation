package graph;
import java.util.Set;
import java.util.Stack;

import binim.PreflowPush;
import java.util.HashSet;

public class FlowNetworkBin {
    private Integer width, height; 
    
    private VertexPreflowSource source;
    private VertexPreflowSink sink;
    private VertexPreflowPixel[][] pixels;

    EdgeFlow[][] sourceConnections;
    EdgeFlow[][] sinkConnections;
    
    EdgeFlow[][] hPenalty;
    EdgeFlow[][] vPenalty;
    
    private Integer flowMax;

    /** Correspond to the function ConstructionReseau
     *  Take in parameter : 
     * @param n the width of the pixel matrix
     * @param m the height of the pixel matrix
     * @param prob_a the probability for a pixel to be in the foreground
     * @param prob_b the probability for a pixel to be in the background
     * @param penalty_horizontal the horizontal penalty 
     * @param penalty_vertical the vertical penalty
     * Complexite O(m*n)
     */
    public FlowNetworkBin(Integer n, Integer m, Integer[][] prob_a, 
        Integer[][] prob_b, Integer[][] penalty_horizontal, 
        Integer[][] penalty_vertical) {
        this.width = n;
        this.height = m;

        this.source = new VertexPreflowSource(0,0);
        this.sink = new VertexPreflowSink(0,0);

        this.sourceConnections = new EdgeFlow[n][m];
        this.sinkConnections = new EdgeFlow[n][m];

        this.pixels = new VertexPreflowPixel[n][m];

        this.hPenalty = new EdgeFlow[n][m-1];
        this.vPenalty = new EdgeFlow[n-1][m];

        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                this.pixels[i][j] = new VertexPreflowPixel(0, 0, i, j);

                this.sourceConnections[i][j] = new EdgeFlow(this.source, this.pixels[i][j], 0, prob_a[i][j]);
                this.sinkConnections[i][j] = new EdgeFlow(this.pixels[i][j], this.sink, 0, prob_b[i][j]);

                
            }                            
        }
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < (m); j++) {
                if( i < (n-1)){
                    this.vPenalty[i][j] = new EdgeFlow(this.pixels[i][j], this.pixels[i+1][j], 0, penalty_vertical[i][j]);
                }
                if(j < (m-1)){
                    this.hPenalty[i][j] = new EdgeFlow(this.pixels[i][j], this.pixels[i][j+1], 0, penalty_horizontal[i][j]);
                }
            }
            
        }
    }
    /**
     * @param v the vertices we want to look at
     * @return the minimal cut of the network's vertices
     * Complexité : O(m*n) BUT only if v is Sink or Source
     * Not used in these cases so in our case -> O(1)
     */

    public Set<EdgeFlow> getNeighborsEdges(VertexPreflow v){
        Set<EdgeFlow> neighbors = new HashSet<EdgeFlow>();
        if(v instanceof VertexPreflowSource) {
            for(Integer i=0; i<this.getWidth();i+=1){
                for(Integer j=0; j<this.getHeight();j+=1){
                    neighbors.add(this.getSourceConnection(i,j));
                }
            }
            return neighbors;
        } else if(v instanceof VertexPreflowSink) {
            for(Integer i=0; i<this.getWidth();i+=1){
                for(Integer j=0; j<this.getHeight();j+=1){
                    neighbors.add(this.getSinkConnection(i,j));
                }
            }
            return neighbors;
        } else {
            // Ajout des connections pixels/source
            // Justification succinte : Nous devons, pour l'algo preflow,
            // pouvoir renvoyer le surplux de courant à la source si besoin
            VertexPreflowPixel pix = (VertexPreflowPixel) v;
            
            neighbors.add(this.getSourceConnection(pix.getX(), pix.getY()));
            neighbors.add(this.getSinkConnection(pix.getX(), pix.getY()));
            

            // Voisinage de Neumann
            

            if(pix.getY() > 0) {
                // On ne touche pas le bord gauche
                neighbors.add(getEdgePenaltyH(pix.getX(), pix.getY() - 1)); // voisin gauche
            }

            if(pix.getY() < this.height - 1) {
                // On ne touche pas le bord droit
                neighbors.add(getEdgePenaltyH(pix.getX() , pix.getY())); // voisin droit
            }

            if(pix.getX() > 0) {
                // On ne touche pas le bord haut
                neighbors.add(getEdgePenaltyV(pix.getX() - 1, pix.getY())); // voisin haut
            }
            
            if(pix.getX() < this.width - 1) {
                // On ne touche pas le bord bas
                neighbors.add(getEdgePenaltyV(pix.getX(), pix.getY())); // voisin bas
            }

            
            return neighbors;
        }
        
    }
    /**
     * @param s the vertex we want to get his neighbors
     * @return a set of these neighbor (only the one we can access, with a residualCapacity > 0)
     * 
     * Time Complexity : O(1)
     */
    public Set<EdgeFlow> getAccessibleNeighborsEdges(VertexPreflow s){
        Set<EdgeFlow> neigh = getNeighborsEdges(s);
        Set<EdgeFlow> accessibleNeigh = new HashSet<EdgeFlow>();
        for (EdgeFlow e : neigh){
            if(e.getCapResiduel(s) !=0){ // Uniquement ceux pour qui on peut acceder car l'arc n'est pas saturé
                accessibleNeigh.add(e);    
            }
        }
        return accessibleNeigh;
    }
    


    /**
     * 
     * @return the maximal flow from source to sink in the network
     */
    public FlowNetworkBin getMaximisedFlow(Boolean quickMode){
        PreflowPush preflow = new PreflowPush(this, quickMode);
        preflow.computePreflow();
        return preflow.flow;
    }

    public Integer getMaximumFlowNumber(){
        return this.flowMax;
    }
    public void setMaximumFlowNumber(Integer f){
        this.flowMax = f;
    }
    /**
     * Depth First Search (Iterative to avoid overflow)
     */
    public Set<VertexPreflowPixel> DFS(VertexPreflow s){ // Classic, no comments
        VertexPreflow current;
        Stack<VertexPreflow> pile = new Stack<>();
        Set<VertexPreflowPixel> DFSSet = new HashSet<VertexPreflowPixel>();
        pile.push(s);
        while(!pile.empty()){
                current = pile.peek();
                pile.pop();  
                if(!current.visited)
                {
                    if(current instanceof VertexPreflowPixel){ // We do not want the source
                        DFSSet.add((VertexPreflowPixel) current);
                    }
                    current.visited = true;
                }
                Set<EdgeFlow> accessibleNeigh = getAccessibleNeighborsEdges(current);
                for(EdgeFlow e : accessibleNeigh){
                    if(!e.getComplementValue(current).visited){
                        pile.push(e.getComplementValue(current));
                    }
                }       
            }
        return DFSSet;
    }

    /** Repport function but not only :
     * Compute the minimal cut inside it
     * and show the solution in the terminal
     * Use almost every functions.
     */
    
    public void displayResult(){

        System.out.println("\n\n-----------------------");
        System.out.println("Results Display");
        System.out.println("----------------------- \n");
        System.out.println("Dimension de la matrice : " + this.getHeight() + "x" + this.getWidth());
        System.out.println("Maximum Flow : " + getMaximumFlowNumber());
        System.out.println("Legend : - = Background");
        System.out.println("         ◼️ = Foreground \n");
        

        Set<VertexPreflowPixel> B = new HashSet<>();
        Set<VertexPreflowPixel> A = DFS(this.getSource()); // Pixel accessible depuis la source dans le réseau résiduel

        for(Integer i=0; i<this.getWidth();i+=1){
            for(Integer j=0; j<this.getHeight(); j++){      
                
            if(A.contains( this.getPixel(i,j))) {
                System.out.print(" ◼️ ");
            }
            else{
                B.add(this.getPixel(i,j));
                System.out.print(" - ");
            }

            }
            System.out.print("\n");
        }
        
        System.out.print("\nBackground's Pixel List : {");
        for(VertexPreflowPixel v : A){
            System.out.print("(" + v.getX() + "," + v.getY() + ")");
        }
        System.out.print("}\nForeground's Pixel List : {");
        for(VertexPreflowPixel v : B){
            System.out.print("(" + v.getX() + "," + v.getY() + ")");
        }
        System.out.print("}\n\n-----------------------");
    }
    /**
     * ACCESSORS O(1)
     */
    public VertexPreflowSource getSource(){return this.source;}
    public VertexPreflowSink getSink(){return this.sink;}
    public VertexPreflowPixel getPixel(Integer i, Integer j){ return this.pixels[i][j];}
    public Integer getWidth(){return this.width;}
    public Integer getHeight(){return this.height;}
    public EdgeFlow getSourceConnection(Integer i, Integer j){
        return this.sourceConnections[i][j];
    }
    public EdgeFlow getSinkConnection(Integer i, Integer j){
        return this.sinkConnections[i][j];
    }
    public EdgeFlow getEdgePenaltyH(Integer i, Integer j){
        return this.hPenalty[i][j];
    }
    public EdgeFlow getEdgePenaltyV(Integer i, Integer j){
        return this.vPenalty[i][j];
    }
}