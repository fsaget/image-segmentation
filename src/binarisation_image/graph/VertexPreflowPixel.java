package graph;
public class VertexPreflowPixel extends VertexPreflow {
    // Un sommet de type pixel
    private Integer x;
    private Integer y; 
    public VertexPreflowPixel(Integer exc, Integer hgt, Integer x, Integer y) {
        super(exc, hgt);
        this.x = x;
        this.y = y;
    }
    public Integer getX(){
        return this.x;
    }
    public Integer getY(){
        return this.y;
    }
}
