package graph;

public class VertexPreflow{
    //Un sommet quelconque possédant une hauteur et un excess

    private Integer height, excess;
    public boolean visited = false; //Uniquement utile et nécessaire pour DFS
    public VertexPreflow(Integer newHeight, Integer newExcess){
        this.height = newHeight;
        this.excess = newExcess;
        assert newHeight >= 0;
        assert newExcess >= 0; 
    }
    public Integer getHeight(){return height;}
    public Integer getExcess(){return excess;}
    
    public void setHeight(Integer newHeight){ 
        assert newHeight > this.height;
        this.height = newHeight;
    }
    public void setExcess(Integer newExcess){ 
        assert newExcess >= 0;
        this.excess = newExcess;
    }
    public void addExcess(Integer toAdd){

        this.excess+= toAdd;
    }
    public void addHeight(Integer toAdd){

        this.height+= toAdd;
    }
}   
